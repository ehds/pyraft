class Ballot(object):
    def __init__(self, conf=None, old_conf=None):
        self._conf = conf
        self._old_conf = old_conf
        self._quorum = len(conf) // 2 + 1 if conf else 0
        self._old_quorum = len(old_conf) // 2 + 1 if old_conf else 0
        self._granted_list = set()

    def grant(self, peer_id) -> None:
        if peer_id in self._granted_list:
            return
        self._granted_list.add(peer_id)

        if peer_id in self._conf:
            self._quorum -= 1
        if (self._old_conf is not None and
                peer_id in self._old_conf):
            self._old_quorum -= 1

    def granted(self) -> bool:
        return self._quorum <= 0 and self._old_quorum <= 0
