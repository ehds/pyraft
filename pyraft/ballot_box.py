""" Used to storage ballot for log entry """


class BallotBox(object):
    def __init__(self) -> None:
        self._last_committed_index = 0
        self._pending_index = 0
