from __future__ import annotations
from typing import List, Tuple, Union

from pyraft.util import EndPoint
from pyraft.logging import create_logger
from pyraft.typing import GroupId

CONFIGURATION_RE = r'(.*?):(\d+):(\d+)'
logger = create_logger(__name__)


class PeerId(object):
    def __init__(self, addr: EndPoint = EndPoint(), idx: int = 0):
        self.addr = addr
        self.idx = idx

    def is_empty(self):
        return (self.addr.ip == EndPoint.IP_ANY and
                self.addr.port == 0 and
                self.idx == 0
                )

    def reset(self):
        self.addr.ip = EndPoint.IP_ANY
        self.addr.port = 0
        self.idx = 0

    def parse(self, peer_str: str):
        import re
        result = re.match(r'(.*?):(\d+):(\d+)', peer_str)
        if result is None or len(result.groups()) != 3:
            # TODO Error formatted
            logger.error(f"Parse from {peer_str} error, not formatted")
            raise ValueError("conf error formatted")
        self.addr.ip, self.addr.port, self.idx = result.groups()

    def __eq__(self, o: PeerId) -> bool:
        return self.addr == o.addr and self.idx == o.idx

    def __ne__(self, o: PeerId) -> bool:
        return not self == o

    def __lt__(self, o: PeerId) -> bool:
        if self.addr < o.addr:
            return True
        else:
            return self.addr == o.addr and self.idx < o.idx

    def __str__(self) -> str:
        return f"{self.addr}:{self.idx}"

    def __hash__(self) -> int:
        return hash(str(self))

# NodeId must implement __eq__ and __hash__
# as dict key


class NodeId(object):
    def __init__(self, group_id: GroupId, peer_id: PeerId):
        self.group_id = group_id
        self.peer_id = peer_id

    def __str__(self):
        return f"{self.peer_id}:{self.group_id}"

    def __eq__(self, o: NodeId) -> bool:
        return self.group_id == o.group_id and\
            self.peer_id == o.peer_id

    def __hash__(self):
        return hash(str(self))


class Configuration(set):
    def __init__(self, peers=[]):
        super().__init__(peers)

    def empty(self) -> bool:
        return len(self) == 0

    def add_peer(self, peer: PeerId) -> None:
        self.add(peer)

    def add_peers(self, peers: List[PeerId]) -> None:
        for peer in peers:
            # or self.add(peer)
            self.add_peer(peer)

    def diffs(self, other: Configuration) -> Tuple[Configuration, Configuration]:
        included = set()
        excluded = set()

        included = self.intersection(other)
        excluded = self - included

        return included, excluded

    def __contains__(self, peers: Union[List[NodeId], NodeId]) -> bool:
        if isinstance(peers, list):
            for peer in peers:
                if not super().__contains__(peer):
                    return False
            return True
        return super().__contains__(peers)

    def parse_from(self, conf: str) -> None:
        self.clear()
        import re

        peers_str = conf.split(',') if conf else []
        for peer_str in peers_str:
            # TODO better match rule
            result = re.match(r'(.*?):(\d+):(\d+)', peer_str)
            if result is None or len(result.groups()) != 3:
                # TODO Error formatted
                logger.error(f"Parse from {conf} error, not formatted")
                self.clear()
                raise ValueError("conf error formatted")
            ip, port, idx = result.groups()
            self.add_peer(PeerId(EndPoint(ip, port), idx))
