from collections import deque
from typing import Deque
from pyraft.configuration import Configuration, PeerId
from pyraft.log_entry import LogEntry


class ConfigurationEntry(object):
    def __init__(self, log_entry: LogEntry = LogEntry()) -> None:
        self.id = log_entry.id
        self.conf = Configuration(log_entry.peers)
        self.old_conf = Configuration(log_entry.old_peers)

    def stable(self):
        return self.old_conf.empty()

    def empty(self):
        return self.conf.empty()

    def get_all_peers(self):
        # TODO need copy ?
        return self.old_conf.union(self.conf)

    def __contains__(self, peer: PeerId) -> bool:
        return (peer in self.conf or
                peer in self.old_conf)


class ConfigurationManager(object):
    def __init__(self) -> None:
        self._configurations: Deque[ConfigurationEntry] = deque()
        self._snapshot: ConfigurationEntry = None

    def add(self, entry: ConfigurationEntry) -> bool:
        if not self._configurations:
            if self._configurations[-1].id >= entry.id:
                # TODO print message
                return False
        self._configurations.append(entry)
        return True

    def truncate_prefix(self, first_keep_index: int):
        while(not self._configurations and
              self._configurations[0].id.index < first_keep_index):
            self._configurations.popleft()

    def truncate_suffix(self, last_keep_index: int):
        while(not self._configurations and
              self._configurations[-1].id.index > last_keep_index):
            self._configurations.pop()

    # TODO setter
    def set_snapshot(self, entry: ConfigurationEntry):
        assert(entry.id >= self._snapshot.id)
        self._snapshot = entry

    def get(self, last_included_index: int) -> ConfigurationEntry:
        """ last included index must less or equal than last index"""
        index = 0
        for item in self._configurations:
            if item.id.index > last_included_index:
                break
            index += 1
        if index == 0:
            return self._snapshot
        # assert(self._configurations[index-1].log_id.index == last_included_index)
        return self._configurations[index - 1]

    def last_configutation(self):
        return self._configurations[-1] if\
            self._configurations else self._snapshot
