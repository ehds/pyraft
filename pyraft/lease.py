from __future__ import annotations
from pyraft.configuration import PeerId
import time
from enum import Enum
from typing import NamedTuple


class Lease(object):
    def __init__(self, timeout) -> None:
        # timeout ms
        assert(timeout > 0)
        self._election_timeout = timeout / 1000  # s float

    @property
    def election_timeout(self):
        return self._election_timeout

    @election_timeout.setter
    def election_timeout(self, election_timeout: int):
        self._election_timeout = election_timeout


class LeaderLease(Lease):
    class State(Enum):
        DISABLED = 1
        EXPIRED = 2
        NOT_READY = 3
        VALID = 4
        SUSPECT = 5

    class LeaseInfo(NamedTuple):
        term: int
        lease_epoch: int
        state: LeaderLease.State

    def __init__(self, timeout) -> None:
        super().__init__(timeout)
        self._last_active_timestamp = 0
        self._term = 0
        self._lease_epoch = 0

    # TODO lock
    def on_leader_start(self, term: int) -> None:
        self._lease_epoch += 1
        self._last_active_timestamp = 0
        self._term = term

    def on_leader_stop(self):
        self._last_active_timestamp = 0
        self._term = 0

    def on_lease_start(self, expected_epoch: int, last_active_timestamp: int) -> None:
        if(self._term == 0 or
           expected_epoch != self._lease_epoch):
            return
        self._last_active_timestamp = last_active_timestamp

    def renew(self, last_active_timestamp: int):
        self._last_active_timestamp = last_active_timestamp

    # TODO more clear
    def get_lease_info(self) -> LeaseInfo:
        if self._term == 0:
            return self.LeaseInfo(0, 0, self.State.EXPIRED)

        if self._last_active_timestamp == 0:
            return self.LeaseInfo(0, 0, self.State.NOT_READY)
        if time.monotonic() < self._last_active_timestamp + self._election_timeout:
            return self.LeaseInfo(self._term, self._lease_epoch, self.State.VALID)
        return self.LeaseInfo(self._term, self._lease_epoch, self.State.SUSPECT)

    @property
    def last_epoch(self):
        return self._lease_epoch


class FollowerLease(Lease):
    def __init__(self, timeout) -> None:
        super().__init__(timeout)
        self._last_leader: PeerId = None
        self._last_leader_timestamp = 0

    def renew(self, peer_id: PeerId):
        self._last_leader = peer_id
        self._last_leader_timestamp = time.monotonic()

    @property
    def last_leader_timestamp(self) -> int:
        return self._last_leader_timestamp

    def voteable_time_from_now(self) -> int:
        now = time.monotonic()  # s float
        voteable_time = self._last_leader_timestamp + self._election_timeout
        if now < voteable_time:
            return 0
        return voteable_time - now

    @property
    def last_leader(self) -> PeerId:
        return self._last_leader

    @property
    def expired(self) -> bool:
        return time.monotonic() - self._last_leader_timestamp >= self._election_timeout

    def expire(self):
        self._last_leader_timestamp = 0

    def reset(self):
        self._last_leader = None
        self._last_leader_timestamp = 0
