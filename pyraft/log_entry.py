from __future__ import annotations

from pyraft.logging import create_logger
from pyraft.service.local_storage_pb2 import ConfigurationPBMeta


class LogId(object):
    def __init__(self, index=0, term=0) -> None:
        self.index = index
        self.term = term

    def __eq__(self, other: LogId) -> bool:
        return self.index == other.index and self.term == other.term

    def __ne__(self, other: LogId) -> bool:
        return not self == other

    def __lt__(self, other: LogId) -> bool:
        if self.term == other.term:
            return self.index < other.index
        return self.term < other.term

    def __gt__(self, other: LogId) -> bool:
        if self.term == other.term:
            return self.index > other.index
        return self.term > other.term

    def __le__(self, other: LogId) -> bool:
        return not self > other

    def __ge__(self, other: LogId) -> bool:
        return not self < other


class LogEntry(object):
    def __init__(self):
        self.type = None
        self.id = LogId(0, 0)
        self.peers = []
        self.old_peers = []
        self.data = None
        self.logger = create_logger(__name__)
        # raft_pb2.RequestVoteRequest()

    def parse_configuration_meta(self, data: bytes) -> LogEntry:
        meta = ConfigurationPBMeta()
        try:
            meta.ParseFromString(data)
        # TODO just raise error?
        except Exception as e:
            self.logger.error("Fail to parse ConfigurationPBMeta", e)
            return None

        entry = LogEntry()
        for peer in meta.peers:
            entry.peers.append(peer)
        for peer in meta.old_peers:
            entry.old_peers.append(peer)
        return entry

    def serialize_configuration_meta(self, entry: LogEntry) -> bytes:
        meta = ConfigurationPBMeta()
        meta.peers.extends(entry.peers)
        meta.old_peers.extends(entry.old_peers)

        meta_bytes = None
        # TODO just raise error?
        try:
            meta_bytes = meta.SerializeToString()
        except Exception as e:
            self.logger.error("Fail to serialize LogEntry", e)
        return meta_bytes
