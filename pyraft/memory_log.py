from typing import List
from .typing import MemoryData
from pyraft.storage import LogStorage
from threading import Lock
from pyraft.log_entry import LogEntry
from collections import deque


class MemoryLogStorage(LogStorage):
    def __init__(self, path="") -> None:
        super().__init__()
        self._path = path
        self._first_log_index = 1
        self._last_log_index = 0
        self._log_entry_data: MemoryData = deque()
        self._lock = Lock()

    def first_log_index(self) -> int:
        with self._lock:
            return self._first_log_index

    def last_log_index(self) -> int:
        with self._lock:
            return self._last_log_index

    def get_entry(self, index: int) -> LogEntry:
        with self.lock:
            if (index < self._first_log_index or
                    index > self._last_log_index):
                return None
            entry = self._log_entry_data[index - self._first_log_index]
        return entry

    def get_term(self, index: int) -> int:
        with self._lock:
            if (index < self._first_log_index or
                    index > self._last_log_index):
                return 0
            temp = self._log_entry_data[index - self._first_log_index]
            assert (temp.id.index == index)
            ret = temp.id.term
        return ret

    def append_entry(self, entry: LogEntry) -> None:
        with self._lock:
            if (entry.id.index != self._last_log_index + 1):
                raise KeyError("input entry index not match log index"
                               f"last log index {self._last_log_index}"
                               f"first log index {self._first_log_index}"
                               f"entry index {entry.id.index}")

            self._log_entry_data.append(entry)
            self._last_log_index += 1

    def append_entries(self, entries: List[LogEntry]) -> None:
        for entry in entries:
            self.append_entry(entry)

    def truncate_prefix(self, first_index_keep: int) -> None:
        with self._lock:
            while len(self._log_entry_data) != 0:
                entry = self._log_entry_data[0]
                if(entry.id.index < first_index_keep):
                    self._log_entry_data.popleft()
                else:
                    break
            self._first_log_index = first_index_keep
            if self._first_log_index > self._last_log_index:
                self._last_log_index = self._first_log_index - 1

    def truncate_suffix(self, last_index_keep: int) -> None:
        with self._lock:
            while len(self._log_entry_data) != 0:
                entry = self._log_entry_data[-1]
                if entry.id.index > last_index_keep:
                    self._log_entry_data.pop()
                else:
                    break
            self._last_log_index = last_index_keep
            if (self._first_log_index > self._last_log_index):
                self._first_log_index = self._last_log_index + 1

    def reset(self, next_log_index: int) -> None:
        if next_log_index <= 0:
            raise KeyError(f"Invalid next_log_index {next_log_index}")
        with self._lock:
            self._log_entry_data.clear()
            self._first_log_index = next_log_index
            self._last_log_index = next_log_index - 1

    def new_instance(self, uri: str) -> LogStorage:
        return MemoryLogStorage(uri)

    def gc_instance(self, uri: str) -> None:
        pass
