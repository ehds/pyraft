from __future__ import annotations, with_statement
from abc import abstractmethod
from pyraft.ballot import Ballot
from pyraft.log_entry import LogId
from threading import Lock, Timer, Thread
from pyraft.raft_meta import FileBasedSingleMetaStorage
from pyraft.logging import create_logger
from pyraft.raft import NodeOptions

from pyraft.timer import RepeatedTimer
from pyraft.configuration import PeerId, NodeId
from pyraft.typing import GroupId
from pyraft.util import EndPoint
from pyraft.configuration_manager import ConfigurationEntry, ConfigurationManager
from pyraft.lease import LeaderLease, FollowerLease
from pyraft.memory_log import MemoryLogStorage
from pyraft.raft import State


class NodeTimer(RepeatedTimer):
    def __init__(self, node: Node, timeout: int) -> None:
        super().__init__(timeout=timeout)
        self._node = node

    @abstractmethod
    def _run(self):
        raise NotImplementedError()


class ElectionTimer(NodeTimer):
    def _run(self):
        self._node.handle_election_timeout()

    def _adjust_time_out(self):
        return super()._adjust_time_out()


class VoteTimer(NodeTimer):
    def _run(self):
        pass

    def _adjust_time_out(self):
        return super()._adjust_time_out()


class StepDownTimer(NodeTimer):
    def _run(self):
        pass

    def _adjust_time_out(self):
        return super()._adjust_time_out()


class Node(object):
    def __init__(self, group_id: GroupId, peer_id: PeerId) -> None:
        self._group_id = group_id
        self._server_id = peer_id
        self._leader_id = PeerId()
        self._current_term = 0
        self._log_storage = None
        self._options = None
        self._conf = ConfigurationEntry()
        self._state = State.STATE_UNINITIALIZED
        self._vote_ctx = self._VoteBallotCtx()
        self._vote_id = PeerId()
        self._lock = Lock()
        self._logger = create_logger(__name__)
        self._node_manager = None

    class _DisruptedLeader(object):
        def __init__(self, peer_id: PeerId = PeerId(), term: int = -1) -> None:
            self._term = term
            self._peer_id = peer_id

    class _GrantSelfArg(object):
        def __init__(self, vote_ctx_version: int, vote_ctx: Node._VoteBallotCtx) -> None:
            self.vote_ctx_version = vote_ctx_version
            self.vote_ctx = vote_ctx

    class _VoteBallotCtx(object):

        def __init__(self) -> None:
            self._timer: Timer = None
            self._triggered = False
            self._version = 0
            self._distrupted_leader = None
            self._reserved_peers = set()
            self._ballot = Ballot()
            self._last_log_id = LogId()

        def init(self, node: Node, triggered: bool) -> None:
            self.reset()
            self._ballot = Ballot(node._conf.conf, node._conf.old_conf)
            self._triggered = triggered

        def reset(self):
            self.stop_grant_self_timer()
            self._version += 1
            self._triggered = False
            self._last_log_id = LogId()
            self._distrupted_leader = Node._DisruptedLeader()
            self._reserved_peers.clear()

        def grant(self, peer_id: PeerId) -> None:
            self._ballot.grant(peer_id)

        def granted(self) -> bool:
            return self._ballot.granted()

        def stop_grant_self_timer(self):
            if self._timer:
                self._timer.cancel()
                self._timer = None

        def handle_grant_timeout(self, arg):
            pass

        def on_grant_timeout(self, arg: Node._GrantSelfArg) -> None:
            # TODO run in thread
            pass

        def start_grant_self_timer(self, due_tim: int) -> None:
            pass

    @property
    def node_id(self) -> NodeId:
        return NodeId(self._group_id, self._server_id)

    @property
    def leader_id(self):
        with self._lock:
            return self._leader_id

    def is_active_state(self, state: State):
        return state.value < State.STATE_ERROR.value

    def init(self, options: NodeOptions) -> None:
        self._options = options

        if self._server_id.addr.ip == EndPoint.IP_ANY:
            self._logger.error("Node can not start from IP_ANY")
            raise ValueError()

        # lazy import (circular import)
        from pyraft.node_manager import NodeManager
        self._node_manager = NodeManager()
        if not self._node_manager.server_exists(self._server_id.addr):
            self._logger.error(f"Group {self._group_id}"
                               f"No RPC server attatched to {self._server_id.addr}"
                               f"Did you for get to add service?")
            raise ValueError()

        self._vote_timer = VoteTimer(self, options.election_timeout)
        self._election_timer = ElectionTimer(self, options.election_timeout)
        self._config_manager = ConfigurationManager()

        # TODO task execute queue init

        self._leader_lease = LeaderLease(options.election_timeout)
        self._follower_lease = FollowerLease(options.election_timeout)

        # TODO extension
        self._log_storage = MemoryLogStorage()
        self._raft_meta_storage = FileBasedSingleMetaStorage("./data/raft_data")

        self._conf.conf = options.initial_conf

        self._state = State.STATE_FOLLOWER
        if not self._conf.empty():
            self.step_down(self._current_term)

        self._node_manager.add(self)

        with self._lock:
            if (self._conf.stable() and
                len(self._conf.conf) == 1 and
                    self._server_id in self._conf.conf):
                # lock release in elect_self
                self.elect_self()
        return
    # in lock

    def elect_self(self) -> None:
        print("elect self")

    def step_down(self, term: int) -> None:
        self._logger.info(f"{self} term {self._current_term}"
                          f" step from {self._state}"
                          f" new_term {term}")

        if not self.is_active_state(self._state):
            self._logger.info("unvalid state")
            return None
        if self._state == State.STATE_CANDIDATE:
            self._vote_timer.stop()
            self._vote_ctx.reset()
        elif self._state == State.STATE_FOLLOWER:
            self._leader_lease.on_leader_stop()

        empty_id = PeerId()
        self.reset_leader_id(empty_id)
        self._state = State.STATE_FOLLOWER

        if term > self._current_term:
            self._vote_id.reset()
            self._raft_meta_storage.set_term_and_votedfor(term, self._vote_id, self._group_id)
        self._election_timer.start()

    def reset_leader_id(self, new_leader_id: PeerId) -> None:
        if new_leader_id.is_empty():
            self._leader_id.reset()
        else:
            self._leader_id = new_leader_id
        return

    def on_grant_self_timeout(self, arg: _GrantSelfArg) -> None:
        Thread(target=self.handle_grant_self_timeout, args=(arg,)).start()

    # in lock
    def grant_self(self, vote_ctx: _VoteBallotCtx) -> None:
        wait_s = self._follower_lease.voteable_time_from_now()
        if wait_s == 0:
            vote_ctx.grant(self._server_id)
            if not vote_ctx.granted():
                return
            else:
                self.become_leader()
            return
        vote_ctx.start_grant_self_timer(wait_s)
    # in lock

    def become_leader(self):
        print("become leader")

    def handle_grant_self_timeout(self, arg: _GrantSelfArg) -> Node:
        with self._lock:
            if not self.is_active_state(self._state):
                return
        self._lock.acquire()
        # release grant self
        self.grant_self(arg.vote_ctx)
        self._lock.release()

    def handle_election_timeout(self) -> None:
        with self._lock:
            if self._state != State.STATE_FOLLOWER:
                return
            if not self._follower_lease.expired:
                return
            empty_id = PeerId()
            self.reset_leader_id(empty_id)

            # TODO pre_vote
            self.elect_self()

    def __str__(self) -> str:
        return f"node {self._group_id} {self._server_id}"
