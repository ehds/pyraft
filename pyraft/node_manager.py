from threading import Lock
from typing import Dict, List, Set
from pyraft.typing import GroupId
import grpc
from pyraft.util import MultiDict, Singleton, EndPoint
from pyraft.configuration import NodeId, PeerId
from pyraft.raft_service import RaftServiceImpl, raft_pb2_grpc
from pyraft.logging import create_logger
from pyraft.node import Node
# one node -> many raft service


class NodeManager(metaclass=Singleton):
    def __init__(self) -> None:
        self._node_map: Dict[NodeId, Node] = dict()
        self._group_map: MultiDict = MultiDict()
        self._lock = Lock()
        self._addr_set: Set[EndPoint] = set()
        self._logger = create_logger(__name__)

    def server_exists(self, addr: EndPoint) -> bool:
        with self._lock:
            # check port is in use
            if addr.ip != EndPoint.IP_ANY:
                any_addr = EndPoint(EndPoint.IP_ANY, addr.port)
                if any_addr in self._addr_set:
                    return True
            return addr in self._addr_set

    def remove_address(self, addr: EndPoint):
        with self._lock:
            if addr in self._addr_set:
                self._addr_set.remove(addr)

    def add_service(self, server: grpc.server, addr: EndPoint) -> None:
        if server is None:
            self._logger.error("grpc server is None")
        if self.server_exists(addr):
            return True

        raft_pb2_grpc.add_RaftServiceServicer_to_server(RaftServiceImpl(addr), server)
        with self._lock:
            self._addr_set.add(addr)

    def add(self, node: Node) -> None:
        node_id = node.node_id
        with self._lock:
            self._node_map[node_id] = node
            if node is not None:
                self._group_map[node_id.group_id] = node

    def remove(self, node: Node) -> None:
        node_id = node.node_id
        with self._lock:
            if (node_id not in self._node_map or
                    self._node_map[node_id] != node):
                return
            self._node_map.pop(node_id)
            for item in self._group_map[node_id.group_id]:
                if item == node:
                    self._group_map[node_id.group_id].remove(item)
                    break
            return

    def get(self, group_id: GroupId, peer_id: PeerId) -> Node:
        node_id = NodeId(group_id, peer_id)
        with self._lock:
            if node_id not in self._node_map:
                return None
            return self._node_map[node_id]

    def get_nodes_by_group_id(self, group_id: GroupId) -> List[Node]:
        # TODO group_map as defaultmap?
        with self._lock:
            if group_id not in self._group_map:
                return []
            return self._group_map[group_id]

    def get_all_ndoes(self) -> List[Node]:
        result = []
        with self._lock:
            for _, nodes in self._group_map.items():
                result.extend(nodes)
        return result
