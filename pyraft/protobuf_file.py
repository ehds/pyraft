from pyraft.logging import create_logger
from pyraft.typing import RPCMessage
import os


class ProtobufFile(object):
    def __init__(self, path: str) -> None:
        self._path = path
        self.logger = create_logger(__name__)

    def save(self, message: RPCMessage) -> None:
        tmp_path = self._path + ".tmp"
        # TODO open with os for better option
        with open(tmp_path, "wb+") as f:
            buf_data = message.SerializeToString()
            # length to bytes, small endian
            length_bytes = len(buf_data).to_bytes(4, "little")
            print(len(buf_data))
            f.write(length_bytes)
            f.write(buf_data)
        os.rename(tmp_path, self._path)

    def load(self, message: RPCMessage) -> None:
        # message must be RPCMessage type
        with open(self._path, "rb") as f:
            content = f.read()
            if len(content) < 4:
                self.logger.error("read file body failed")
                # TODO self define FileBodyError
                raise ValueError("read file body error")

            length_bytes = content[:4]
            length = int.from_bytes(length_bytes, "little")
            if len(content) - 4 != length:
                self.logger.error("read file body failed")
                # TODO self define FileBodyError
                raise ValueError("read file body error")
            message.ParseFromString(content[4:])
