from enum import Enum
from pyraft.configuration import Configuration


class NodeOptions(object):
    def __init__(self) -> None:
        self.election_timeout = 1000
        self.log_storage = None
        self.initial_conf = Configuration()
        self.raft_meta_uri = ""


class State(Enum):
    STATE_LEADER = 1
    STATE_TRANSFERRING = 2
    STATE_CANDIDATE = 3
    STATE_FOLLOWER = 4
    STATE_ERROR = 5
    STATE_UNINITIALIZED = 6
    STATE_SHUTTING = 7
    STATE_SHUTDOWN = 8
    STATE_END = 9
