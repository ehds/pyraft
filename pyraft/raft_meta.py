import os
from typing import Tuple
from .typing import VersionedGroupId
from pyraft.configuration import PeerId
from pyraft.logging import create_logger
from pyraft.storage import RaftMetaStorage, gc_dir
from pyraft.protobuf_file import ProtobufFile
from pyraft.service.local_storage_pb2 import StablePBMeta


class FileBasedSingleMetaStorage(RaftMetaStorage):
    RAFT_META_NAME = "raft_meta"

    def __init__(self, path: str) -> None:
        self._path = path
        self._inited = False
        self._term = 1
        self._votedfor = PeerId()
        self.logger = create_logger(__name__)

    def init(self) -> bool:
        if self._inited:
            return True
        dir_path = self._path
        if os.path.exists(dir_path):
            return True
        os.mkdir(dir_path)

        self._load()
        self._inited = True

    def _load(self) -> None:
        path = os.path.join(self._path, FileBasedSingleMetaStorage.RAFT_META_NAME)
        if not os.path.exists(path):
            self.logger.warning(f"{path} not exists")
            # first init, there maybe not exists
            return

        meta = StablePBMeta()
        pb_file = ProtobufFile(path)
        pb_file.load(meta)
        self._term = meta.term
        self._votedfor.parse(meta.votedfor)

    def set_term_and_votedfor(self, term: int, peer_id: PeerId, group_id) -> None:
        if not self._inited:
            self.logger.error("SingleMetaStorage not init path {self._path}")
            return
        self._term = term
        self._votedfor = peer_id
        self._save()

    def _save(self):
        meta = StablePBMeta
        meta.term = self._term
        meta.votedfor = str(self._votedfor)

        path = os.path.join(self._path, FileBasedSingleMetaStorage.RAFT_META_NAME)
        pb_file = ProtobufFile(path)
        pb_file.save(meta)

        self.logger.info(f"Saved single stable meta, path {self._path}"
                         "term {self._term}"
                         "votedfor {self._votedfor}")

    def get_term_and_votedfor(self) -> Tuple[int, PeerId]:
        if not self._inited:
            self.logger.error("SingleMetaStorage not init path {self._path}")
            return
        return self._term, self._votedfor

    def new_instance(self, uri: str) -> RaftMetaStorage:
        return FileBasedSingleMetaStorage(uri)

    def gc_instance(self, uri: str, vgid: VersionedGroupId) -> None:
        gc_dir(uri)
        self.logger.info(f"Group {vgid} succed to gc single stable storage"
                         " path {uri}")
