import grpc
from pyraft.service import raft_pb2, raft_pb2_grpc
from pyraft.util import EndPoint


class RaftServiceImpl(raft_pb2_grpc.RaftService):
    def __init__(self, addr: EndPoint) -> None:
        self._addr = addr

    def pre_vote(request: raft_pb2.RequestVoteRequest,
                 context: grpc.ServicerContext) -> raft_pb2.RequestVoteResponse:
        pass

    def request_vote(request: raft_pb2.RequestVoteRequest,
                     context: grpc.ServicerContext) -> raft_pb2.RequestVoteResponse:
        pass

    def append_entries(request: raft_pb2.AppendEntriesRequest,
                       context: grpc.ServicerContext) -> raft_pb2.AppendEntriesResponse:
        pass

    def install_snapshot(request: raft_pb2.InstallSnapshotRequest,
                         context: grpc.ServicerContext) -> raft_pb2.InstallSnapshotResponse:
        pass

    def timeout_now(request: raft_pb2.TimeoutNowRequest,
                    context: grpc.ServicerContext) -> raft_pb2.TimeoutNowResponse:
        pass
