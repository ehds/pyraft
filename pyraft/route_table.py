
import grpc
from typing import Dict, Union

from pyraft.typing import GroupId
from pyraft.util import Singleton
from pyraft.configuration import Configuration, PeerId
from pyraft.logging import create_logger

from pyraft.service import cli_pb2, cli_pb2_grpc
logger = create_logger(__name__)

# TODO clear code


class RouteTable(metaclass=Singleton):
    class GroupConf(object):
        __slots__ = ["leader", "conf"]
        leader: PeerId
        conf: Configuration

    def __init__(self) -> None:
        self._group_map: Dict[GroupId, RouteTable.GroupConf] = {}

    def update_conf(self, group: GroupId, conf: Configuration):
        if group not in self._group_map:
            gc = RouteTable.GroupConf()
            gc.conf = conf
            gc.leader = PeerId()
            self._group_map[group] = gc

        gc = self._group_map[group]
        if(not gc.leader.is_empty() and gc.leader not in gc.conf):
            self._group_map[group].leader.reset()

    def update_leader(self, group: GroupId, leader: PeerId):
        if group not in self._group_map:
            return
        self._group_map[group].leader = leader

    def remove_group(self, group: GroupId):
        if group in self._group_map:
            del self._group_map[group]
            # self._group_map.pop(group)

    def select_leader(self, group: GroupId) -> Union[None, PeerId]:
        if group not in GroupId:
            return None
        gc = self._group_map[group]
        if gc.leader.is_empty():
            return None
        return gc.leader

    def get_conf(self, group: GroupId) -> Configuration:
        if group not in self._group_map:
            return None
        return self._group_map[group].conf


def update_configuration(group: GroupId, conf: Union[Configuration, str]):
    rb = RouteTable()
    if isinstance(conf, Configuration):
        rb.update_conf(group, conf)
    elif isinstance(conf, str):
        cf = Configuration()
        cf.parse_from(conf)
        rb.update_conf(group, cf)
    else:
        raise ValueError(f"Can't update configuration of {conf}")
    return


def update_leader(group: GroupId, leader: PeerId):
    rb = RouteTable()
    rb.update_leader(group, leader)


def refresh_leader(group: GroupId) -> PeerId:
    rb = RouteTable()
    conf = rb.get_conf(group)
    if not conf:
        return None

    for peer in conf:
        with grpc.insecure_channel(str(peer.addr)) as channel:
            stub = cli_pb2_grpc.CliServiceStub(channel)
            request = cli_pb2.GetLeaderRequest()
            request.group_id = group
            # TODO maybe raise error, try...except ?
            try:
                response: cli_pb2.GetLeaderResponse = stub.get_leader(request)
            except grpc.RpcError:
                logger.error(f"Can not get leader info from {peer.addr}, RPC error.")
                continue
            else:
                update_leader(response.id)


def select_leader(group: GroupId) -> PeerId:
    rb = RouteTable()
    return rb.select_leader(group)
