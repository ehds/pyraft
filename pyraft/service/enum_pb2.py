# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: enum.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='enum.proto',
  package='pyraft',
  syntax='proto2',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\nenum.proto\x12\x06pyraft*l\n\tEntryType\x12\x16\n\x12\x45NTRY_TYPE_UNKNOWN\x10\x00\x12\x14\n\x10\x45NTRY_TYPE_NO_OP\x10\x01\x12\x13\n\x0f\x45NTRY_TYPE_DATA\x10\x02\x12\x1c\n\x18\x45NTRY_TYPE_CONFIGURATION\x10\x03*\x82\x01\n\tErrorType\x12\x13\n\x0f\x45RROR_TYPE_NONE\x10\x00\x12\x12\n\x0e\x45RROR_TYPE_LOG\x10\x01\x12\x15\n\x11\x45RROR_TYPE_STABLE\x10\x02\x12\x17\n\x13\x45RROR_TYPE_SNAPSHOT\x10\x03\x12\x1c\n\x18\x45RROR_TYPE_STATE_MACHINE\x10\x04'
)

_ENTRYTYPE = _descriptor.EnumDescriptor(
  name='EntryType',
  full_name='pyraft.EntryType',
  filename=None,
  file=DESCRIPTOR,
  create_key=_descriptor._internal_create_key,
  values=[
    _descriptor.EnumValueDescriptor(
      name='ENTRY_TYPE_UNKNOWN', index=0, number=0,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='ENTRY_TYPE_NO_OP', index=1, number=1,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='ENTRY_TYPE_DATA', index=2, number=2,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='ENTRY_TYPE_CONFIGURATION', index=3, number=3,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=22,
  serialized_end=130,
)
_sym_db.RegisterEnumDescriptor(_ENTRYTYPE)

EntryType = enum_type_wrapper.EnumTypeWrapper(_ENTRYTYPE)
_ERRORTYPE = _descriptor.EnumDescriptor(
  name='ErrorType',
  full_name='pyraft.ErrorType',
  filename=None,
  file=DESCRIPTOR,
  create_key=_descriptor._internal_create_key,
  values=[
    _descriptor.EnumValueDescriptor(
      name='ERROR_TYPE_NONE', index=0, number=0,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='ERROR_TYPE_LOG', index=1, number=1,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='ERROR_TYPE_STABLE', index=2, number=2,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='ERROR_TYPE_SNAPSHOT', index=3, number=3,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='ERROR_TYPE_STATE_MACHINE', index=4, number=4,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=133,
  serialized_end=263,
)
_sym_db.RegisterEnumDescriptor(_ERRORTYPE)

ErrorType = enum_type_wrapper.EnumTypeWrapper(_ERRORTYPE)
ENTRY_TYPE_UNKNOWN = 0
ENTRY_TYPE_NO_OP = 1
ENTRY_TYPE_DATA = 2
ENTRY_TYPE_CONFIGURATION = 3
ERROR_TYPE_NONE = 0
ERROR_TYPE_LOG = 1
ERROR_TYPE_STABLE = 2
ERROR_TYPE_SNAPSHOT = 3
ERROR_TYPE_STATE_MACHINE = 4


DESCRIPTOR.enum_types_by_name['EntryType'] = _ENTRYTYPE
DESCRIPTOR.enum_types_by_name['ErrorType'] = _ERRORTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)


# @@protoc_insertion_point(module_scope)
