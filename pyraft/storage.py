from __future__ import annotations
import os
from abc import ABC, abstractmethod
from typing import List, Tuple
from pyraft.typing import VersionedGroupId
from pyraft.log_entry import LogEntry
from pyraft.configuration import PeerId


class LogStorage(ABC):
    def __init__(self) -> None:
        super().__init__()

    @abstractmethod
    def first_log_index(self) -> int:
        pass

    @abstractmethod
    def last_log_index(self) -> int:
        pass

    @abstractmethod
    def get_entry(self, index: int) -> LogEntry:
        pass

    @abstractmethod
    def get_term(self, index: int) -> int:
        pass

    @abstractmethod
    def append_entry(self, entry: LogEntry) -> None:
        pass

    @abstractmethod
    def append_entries(self, entries: List[LogEntry]) -> None:
        pass

    @abstractmethod
    def truncate_prefix(self, first_index_keep: int) -> None:
        pass

    @abstractmethod
    def truncate_suffix(self, last_index_keep: int) -> None:
        pass

    @abstractmethod
    def reset(self, next_log_index: int) -> None:
        pass

    @abstractmethod
    def new_instance(self, uri: str) -> LogStorage:
        pass

    @staticmethod
    def create(cls, uri: str) -> LogStorage:
        pass

    # TODO pass or rais NotImplementedError
    @abstractmethod
    def gc_instance(self, uri: str) -> None:
        raise NotImplementedError("gc_instance interface is not implemented")


class RaftMetaStorage(ABC):
    def __init__(self) -> None:
        super().__init__()

    @abstractmethod
    def set_term_and_votedfor(self, term: int, peer_id: PeerId, group_id=VersionedGroupId) -> None:
        pass

    @abstractmethod
    def get_term_and_votedfor(self) -> Tuple[int, PeerId]:
        pass

    @abstractmethod
    def new_instance(self, uri: str) -> RaftMetaStorage:
        pass

    @staticmethod
    def create(self, uri: str) -> RaftMetaStorage:
        pass

    @abstractmethod
    def gc_instance(self, uri: str, vgid: VersionedGroupId) -> None:
        raise NotImplementedError(f"{self.__class__} didn't implement gc_instanc interface")


def gc_dir(path):
    target_path, tmp_path = path, path + ".tmp"

    def delete_file(path):
        if os.path.isdir(path):
            import shutil
            shutil.rmtree(path)
        elif os.path.isfile(path):
            os.remove(path)
        elif os.path.islink(path):
            os.unlink(path)

    delete_file(tmp_path)
    # avoid deleting faied, target_path must be deleted atomic
    os.replace(target_path, tmp_path)
    delete_file(target_path)
