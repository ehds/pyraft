from abc import ABC, abstractmethod
from threading import Timer, Lock
import threading


class RepeatedTimer(ABC):
    def __init__(self, timeout: int = 0) -> None:
        self._timeout = timeout / 1000
        self._stopped = True
        self._destoryed = False
        self._running = False
        self._invoking = False
        self._timer: Timer = None
        self._lock = Lock()

    def start(self):
        with self._lock:
            if (not self._stopped or
                    self._destoryed):
                return
            self._stopped = False
            # timer was not deleted by last task
            # so maybe it is running
            if self._running:
                return
            self._running = True
            self.schedule()

    @abstractmethod
    def _run(self):
        pass

    def _adjust_time_out(self):
        return self._timeout

    def stop(self):
        with self._lock:
            self._stopped = True
            assert self._running
            self._timer.cancel()
            self._running = False

    def on_time_out(self):
        with self._lock:
            self._invoking = True

        self._run()
        with self._lock:
            self._invoking = False
            if self._stopped:
                self._running = False
                return
            self.schedule()

    def run_timeout_in_new_thread(self):
        threading.Thread(target=self.on_time_out, args=()).start()

    # in lock
    def schedule(self):
        self._timer = Timer(
            self._timeout, self.run_timeout_in_new_thread, args=())
        self._timer.start()

    def reset(self, timeout: int = None):
        with self._lock:
            if timeout:
                self._timeout = timeout
            if self._stopped:
                return
            # assert self._running
            self._timer.cancel()
            self.schedule()

    def cancle(self):
        with self._lock:
            if self._destoryed:
                return
            self._destoryed = True
            if not self._running:
                assert(self._stopped)
                return
            if self._stopped:
                return
            self._stopped = True
            # this timer is running
            self._timer.cancel()
            self._running = False
