# import typing

from typing import Deque
from google.protobuf.message import Message
from pyraft.log_entry import LogEntry

RPCMessage = Message
GroupId = str
VersionedGroupId = str
MemoryData = Deque[LogEntry]
