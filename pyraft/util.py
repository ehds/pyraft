from __future__ import annotations
import time
from threading import Lock
from typing import List, TypeVar


class EndPoint(object):
    __slots__ = ["ip", "port"]
    IP_ANY = "0.0.0.0"

    def __init__(self, ip: str = IP_ANY, port: int = 0):
        self.ip = ip
        self.port = port

    def __eq__(self, o: EndPoint) -> bool:
        return EndPoint.ip2int(self.ip) == EndPoint.ip2int(o.ip) and\
            self.port == o.port

    def __ne__(self, o: EndPoint) -> bool:
        return not self == o

    def __lt__(self, o: EndPoint) -> bool:
        if EndPoint.ip2int(self.ip) < EndPoint.ip2int(o.ip):
            return True
        else:
            return EndPoint.ip2int(self.ip) == EndPoint.ip2int(o.ip) and\
                self.port < o.port

    def __str__(self) -> str:
        return f"{self.ip}:{self.port}"

    def __hash__(self) -> int:
        return hash(str(self))

    @staticmethod
    def ip2int(ip: str) -> int:
        h = list(map(int, ip.split(".")))
        return (h[0] << 24) + (h[1] << 16) + (h[2] << 8) + (h[3] << 0)

    @staticmethod
    def int2ip(ip: int) -> str:
        return ".".join(map(str, [((ip >> 24) & 0xff), ((ip >> 16) & 0xff), ((ip >> 8) & 0xff), ((ip >> 0) & 0xff)]))


def now_ms():
    return int(time.time() * 1000)


def now_s():
    return time.time()


class Singleton(type):
    _instances = {}
    _lock = Lock()

    def __call__(cls, *args, **kwgs):
        if cls not in cls._instances:
            with cls._lock:
                if cls not in cls._instances:
                    cls._instances[cls] = super().__call__(*args, **kwgs)
        return cls._instances[cls]


_KT = TypeVar('_KT')
_VT = TypeVar('_VT')

class MultiDict(dict):
    def __setitem__(self, k: _KT, v: _VT):
        if k not in self:
            super().__setitem__(k, [v])
        else:
            self[k].append(v)

    def __getitem__(self, k: _KT) -> List[_VT]:
        return super().__getitem__(k)
