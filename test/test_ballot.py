import unittest
from pyraft import Configuration, Ballot


class BallotGrantTestCase(unittest.TestCase):
    def setUp(self):
        self.conf = Configuration()
        self.old_conf = Configuration()
        self.conf.parse_from(
            "127.0.0.1:8001:0,127.0.0.1:8002:0,127.0.0.1:8003:0")
        self.old_conf.parse_from(
            "127.0.0.1:8004:0,127.0.0.1:8005:0,127.0.0.1:8006:0")

    def test_grant_all(self):
        ballot = Ballot(self.conf, self.old_conf)
        for peer in self.conf:
            ballot.grant(peer)
        self.assertFalse(ballot.granted())
        for peer in self.old_conf:
            ballot.grant(peer)
        self.assertTrue(ballot.granted())

    def test_grant_majority(self):
        ballot = Ballot(self.conf)
        quorum = len(self.conf) // 2 + 1
        for peer in self.conf:
            ballot.grant(peer)
            quorum -= 1
            if(quorum > 0):
                self.assertFalse(ballot.granted())
            else:
                self.assertTrue(ballot.granted())
