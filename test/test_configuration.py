import unittest
from pyraft import Configuration


class ConfigurationTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def test_parse_from_empty_str(self):
        conf = Configuration()
        conf.parse_from("")
        self.assertEqual(len(conf), 0)

    def test_parse_from_str(self):
        conf = Configuration()
        conf.parse_from(
            "127.0.0.1:8001:0,127.0.0.1:8002:0,127.0.0.1:8003:0")
        self.assertEqual(len(conf), 3)

    def test_parse_value_error(self):
        conf = Configuration()
        with self.assertRaises(ValueError):
            conf.parse_from(
                "127.0.0.1:8001,127.0.0.1:0,127.0.0.1:8003:0")
