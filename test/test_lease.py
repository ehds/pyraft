import unittest
from pyraft import LeaderLease
import time


class LeaseTestCase(unittest.TestCase):
    def test_leader_lease(self):
        excepted_term = 1
        leader_lease = LeaderLease(3000)

        leader_lease.on_leader_start(excepted_term)
        expected_epoch = leader_lease.last_epoch

        self.assertEqual(leader_lease.get_lease_info().state,
                         LeaderLease.State.NOT_READY)

        # in election timeout
        leader_lease.on_lease_start(expected_epoch, time.monotonic())
        time.sleep(1)
        self.assertEqual(leader_lease.get_lease_info().state,
                         LeaderLease.State.VALID)
        # timeout
        time.sleep(3)
        self.assertEqual(leader_lease.get_lease_info().state,
                         LeaderLease.State.SUSPECT)

        # stop leader lease
        leader_lease.on_leader_stop()
        self.assertEqual(leader_lease.get_lease_info().state,
                         LeaderLease.State.EXPIRED)
