import unittest
import grpc
from concurrent.futures import ThreadPoolExecutor
from pyraft.util import EndPoint
from pyraft.configuration import PeerId
from pyraft.node import Node
from pyraft.node_manager import NodeManager


class NodeManagerTestCase(unittest.TestCase):
    def setUp(self):
        self._server = grpc.server(ThreadPoolExecutor())
        self._addr = EndPoint("127.0.0.1", 80)
        self._node_manager = NodeManager()

    def test_add_node_manager(self):
        group_id = "0"
        peer_id = PeerId(self._addr, 0)

        self._node_manager.add_service(self._server, self._addr)
        node = Node(group_id, peer_id)
        self._node_manager.add(node)

        # node_id = NodeId(group_id,peer_id)
        node_find = self._node_manager.get(group_id, peer_id)
        self.assertEqual(node.node_id, node_find.node_id)

        nodes = self._node_manager.get_nodes_by_group_id(group_id)
        self.assertEqual(len(nodes), 1)

        nodes = self._node_manager.get_all_ndoes()
        self.assertEqual(len(nodes), 1)

        self._node_manager.remove(node)
        nodes = self._node_manager.get_all_ndoes()
        self.assertEqual(len(nodes), 0)
