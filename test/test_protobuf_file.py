import unittest
from pyraft.protobuf_file import ProtobufFile
from pyraft.service import cli_pb2


class ProtobufFileTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._path = "/tmp/test.txt"

    def test_save_protobuf(self):
        request_save = cli_pb2.GetLeaderRequest()
        request_save.group_id = "123中文abc\t\n"
        pbf = ProtobufFile(self._path)
        pbf.save(request_save)

        request_load = cli_pb2.GetLeaderRequest()
        pbf.load(request_load)
        self.assertEqual(request_load.group_id, request_save.group_id)
