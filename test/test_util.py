import unittest
from pyraft.util import MultiDict


class UtilTestCase(unittest.TestCase):
    def test_multi_map(self):
        mm = MultiDict()
        self.assertEqual(len(mm), 0)
        self.assertTrue('a' not in mm)
        # add multiple value to same name
        mm['a'] = 1
        mm['a'] = 2
        self.assertEqual(len(mm), 1)
        self.assertEqual(len(mm['a']), 2)
        self.assertEqual(mm['a'][0], 1)
        self.assertEqual(mm['a'][1], 2)

        # remove value
        mm['a'].remove(mm['a'][0])
        self.assertEqual(len(mm), 1)
        self.assertEqual(len(mm['a']), 1)
        self.assertEqual(mm['a'][0], 2)
